FROM node
EXPOSE 8080
COPY . /app
WORKDIR /app
RUN apt-get update -yq   &&  apt-get install -yq curl  
 
RUN curl -sL https://deb.nodesource.com/setup |  bash - &&  apt-get install -yq nodejs build-essential
RUN cd /app; npm install
CMD ["node", "index.js"]



